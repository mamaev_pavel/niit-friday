// ��������� ��������� ������ ������������ � ����������� ��������
// ������ ������� ����������� �� ������������ �����
// ��������� ���������� ����� ���������� ������. � ������. ��������
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <locale.h>
typedef unsigned long long ULL;
ULL sumRec(ULL *pStart, ULL *pEnd);
ULL sumTr(ULL *pStart, ULL *pEnd);
void fill(ULL *pStart, ULL *pEnd);
void printArr(ULL *pStart, ULL *pEnd);
int main()
{
	setlocale(LC_CTYPE, "RUS");
	srand(time(NULL) | clock());
	clock_t time_start, time_end;
	ULL N;
	unsigned int M;
	ULL *arr;
	double tRec, tTr;
	printf("������ ������� ������������ 2 � ������� M\n������� ���� ����� M _ ");
	scanf("%u", &M);
	N = pow(2, M);
	arr = (ULL*)malloc(N * sizeof(ULL));            // ��������� ������
	fill(arr, arr + N);     
	//printArr(arr, arr + N);     

	time_start = clock();
	printf("\nsumRec = %llu", sumRec(arr, arr + N - 1));
	time_end = clock();
	printf(" t _ %.3lfc\n", tRec = (double)(time_end - time_start) / CLOCKS_PER_SEC);

	time_start = clock();
	printf("sumTr = %llu", sumTr(arr, arr + N - 1));
	time_end = clock();
    printf(" t _ %.3lfc\n", tTr = (double)(time_end - time_start) / CLOCKS_PER_SEC);

	printf("faster ");            // ��� �������
	if (tTr < tRec)
		puts("traditional");
	else if (tRec < tTr)
		puts("recursive");
	else  puts("equal");
	
	free(arr);       // ������������ ������
	return 0;
}

void fill(ULL *pStart, ULL *pEnd)      // ���������� �������
{
	for (; pStart < pEnd; pStart++)
		*pStart = rand() % 100;
}
void printArr(ULL *pStart, ULL *pEnd)    // ������ ������� ��� ��������
{
	for (; pStart < pEnd; pStart++)
		printf("%d ", *pStart);
}
ULL sumRec(ULL *pStart, ULL *pEnd)        // ����������� �����
{
	if (pStart == pEnd)
		return *pStart;
	else
		return sumRec(pStart, pStart + (pEnd - pStart) / 2) + sumRec(pStart + (pEnd - pStart) / 2 + 1, pEnd);
}
ULL sumTr(ULL *pStart, ULL *pEnd)          // ������������
{
	
	ULL sum=0;
	for (; pStart < pEnd; pStart++, pEnd--)
		sum += *pStart + *pEnd;
	
	return sum;

}