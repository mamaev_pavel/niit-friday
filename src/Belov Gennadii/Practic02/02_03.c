// ��������� ���������� �� ������ ����������� �������� ������(���-�� �����) �� �������� ��������
#include <stdio.h>
#include <string.h>
#include <locale.h>

int main(void) 
{
	setlocale(LC_CTYPE, "");

	int qtStr, i, j, qtSpace;
	char str[50];
	char space = ' ';

	printf("������� ������(�): ");
	fgets(str, 50, stdin);       // ��������� ������ (��� ������� Enter ����������� ������ ����� ������ - \n)
	qtSpace = strlen(str) - 1;  // ���-�� �������� ����� '_'
	str[qtSpace] = '\0';       // �������� \n
	
	printf("������� ����� �����: ");
	scanf("%d", &qtStr);

	for (i = 0; i<qtStr; i++)
	{
		for (j = qtStr - 1; j>i; j--)
			printf("%*c", qtSpace, space);

		for (j = 0; j <= i*2; j++)
		{
			printf("%s", str);
		}
		printf("\n");
	}
	
	return 0;
}