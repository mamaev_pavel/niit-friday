/*�������� ���������, ������������ � ������ ���������� ������������������
������������ �����
���������:
��� ������ AABCCCDDEEEEF ��������� 4 � EEEE*/
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

int main()
{
	int i = 0;
	int j = 0;
	int contr = 0;
	int povtor = 1; //������ �������� ��������
	int maxpovtor = 0; //������������ ������ ��������
	int len = 0; //������ ������
	char str[81];

	printf("Enter the string (1-80):\n");
	fgets(str, 80, stdin);
	len = strlen(str);

	for (i = 1; i != len; i++,j++) //j ����� �������� �� i-1
	{
		if (str[i] == str[j])
		{
			povtor++;

		}
		else if (povtor > maxpovtor)
		{
			maxpovtor = povtor;
			povtor = 1;
			contr = j;
		}
		else povtor = 1;
	}
	printf("\n ");
	printf("The number of repeats %d - ", maxpovtor);
	while (maxpovtor != 0)     //����� ������������������ ��������
	{
		putchar(str[contr]);
		contr--;
		maxpovtor--;
	}
	printf("\n ");
	return 0;
}