/*�������� ���������, ������� ������� �� ����� 10 �������, ���������������
��������� ������� �� ��������� ���� � ����, ������ ����� ������ ����
��� � ������, ��� � � ������� ���������. ����� ������ - 8 ��������.
���������:
������ ���������������� ������: Nh1ku83k
*/

#include <stdio.h>
#include <string.h>
#include <Windows.h>
#include <time.h>
#include <stdlib.h>

int main()
{
	int n = 0;
	int s = 0;
	srand(time(0));
  
	while (n != 10)
	{
		while (s != 8)
		{
			switch (rand() % 3)
			{
			case 0:
				printf("%c", 'a' + rand() % 26);
				break;
			case 1:
				printf("%c", 'A' + rand() % 26 );
				break;
			case 2:
				printf("%c", '0' + rand() % 10);
				break;
			}
			s++;
		}
		s = 0;
		n++;
		printf("\n");
	}
	return 0;
}
