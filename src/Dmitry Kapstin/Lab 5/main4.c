/*�������� ���������, ������� ������ ��������� ��������� ���� �
������������ �������� ����� � ������ ������
���������:
��������� ��������� ������������ ���������� ���� � ������ ��� �������-
��. ��� ������ ������ ���������� �������, ������������� � ������ ������
1*/
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#define M 80

int getWord(char **words, char *str) //����������� ������� � ���������� �������� ������� ����� � ������
{
	int inWord = 0; int i = 0, j = 0;
	while (str[i] != '\n')
	{
		if (str[i] != ' ' && inWord == 0)
		{
			inWord = 1;
			words[j] = &str[i];
			j++;
		}
		else if (str[i] == ' ' && inWord == 1)
		{
			inWord = 0;
			
		}
		i++;
	}
	return j;
}
void MixWords(char **words, int j) // ������������� ��������� ������� �������� ������� ����� ����� 1 � ����������
{
	int k = 0, w = 0;
	char *temp;
	if (j< 2) return 0;
	k = rand() % j;
	temp = words[j-1];
	words[j-1] = words[k];
	words[k] = temp;
	j--;
	MixWords(words, j);
}

int main()
{
	char str[M][M];
	char *words[M];
	int i = 0, n = 0, count = 0, string = 0;
	srand(time(0));
	FILE *in, *out;
	in = fopen("input1.txt", "r");
	out = fopen("output1.txt", "w");
	if (in == 0 || out == 0)
	{
		puts("File error");
		return 1;
	}
	while (fgets(str[i], M, in)) //��� ������������ �������� �����
		words[i] = str[i++];

	string = i; // ���-�� �����
	i = 0; // ��������� �����

	while (i < string)
	{
		count=getWord(words, str[i]);
		MixWords(words, count );
		for (n = 0; n < count; n++)
		{
			while ((*words[n] != ' ') && (*words[n] != 0) && (*words[n] != '\n'))
			{
				fprintf(out, "%c", *words[n]);
				*words[n]++;
			}
			fprintf(out, " ");
		}
		fprintf(out, "\n");
		i++;
	}
	fclose(in);
	fclose(out);
	return 0;
}