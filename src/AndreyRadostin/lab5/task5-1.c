//This program changes the order of words in user's line

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#define N 80
void printWord(char *p);
int getWords(char *str, char **p);

int main()
{
	int i = 0, k,numb_words;
	char str[N], *p[N], *temp;
	printf("Please, print something \n");
	fgets(str, 80, stdin);

	i = strlen(str) - 1;
	str[i] = ' ';
	srand(time(0));
	numb_words = getWords(str, p);
	
	for (i = numb_words; i > 0; --i)
	{
		k = rand() % i;
		printWord(p[k]);
		temp = p[k];
		p[k] = p[i - 1];
		p[i - 1] = temp;
	}
	return 0;
}
int getWords(char *str, char **p)
{
	int i = 0, count = 0, inWord = 0;
	while (str[i])
	{
		if (str[i] != ' ' && inWord == 0)
		{
			p[count] = str + i;
			count++;
			inWord = 1;
		}
		else if (str[i] == ' ' && inWord == 1)
			inWord = 0;
		i++;
	}
	return count;
}
void printWord(char *p)
{
	while (*p != ' ')
	{
		putchar(*p++);
	}
	putchar(' ');
}