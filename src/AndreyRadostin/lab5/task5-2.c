//Kaleidoscope

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <windows.h>
#define N 24

void Clean(char(*str)[N]);
void DRAW(char(*str)[N]);
void Fill(char(*str)[N]);
void Reflection(char(*str)[N]);

int main()
{
	int i = 0, j=0;
	char str[N][N];
	for (;;)
	{
		srand(time(0));
		Clean(str);
		Fill(str);
		Reflection(str);
		DRAW(str);
		Sleep(5000);
	}
	
	return 0;
	
}
void DRAW(char(*str)[N])
{
	int i = 0, j = 0;
	for (j = 0; j < N; ++j)
	{
		for (i = 0; i < N; ++i)
			printf("%c", str[j][i]);
		printf("\n");
	}
	
}

void Clean(char(*str)[N])
{
	int i = 0, j=0;
	
	for (j = 0; j < N; ++j)
	for (i = 0; i < N; ++i)
		str[j][i] = ' ';
}
void Fill(char(*str)[N])
{
	int i;
	for (i = 0; i < N * 2; ++i)
	{
		str[rand() % (N / 2)][rand() % (N / 2)] = '*';

	}
}

void Reflection(char(*str)[N])
{
	int i = 0, j = 0;

	for (j = 0; j < N/2; ++j)
	for (i = 0; i < N/2; ++i)
	if (str[j][i] == '*')
	{
		str[j][N - i-1] = '*';
		str[N - j-1][i] = '*';
		str[N - j-1][N-i-1] = '*';
	}
}