//This program finds the longest word in user's line

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 80


int main()
{
	int i = 0, count = 0, inWord = 0, j = 0,temp_l=0;
	int wordstart = 0, wordstart_m = 0;
	char str[N];
	printf("Please, print something \n");
	fgets(str, 80, stdin);

	i = strlen(str) - 1;
	str[i + 1] = '\0';
	str[i] = ' ';
	i = 0;
	
	while (str[i] != '\0')
	{
		if (str[i] != ' ' && inWord == 0)
		{
			inWord = 1; wordstart = i;
		}
		else if (str[i] == ' ' && inWord == 1)
		{
			inWord = 0;
			if (i - wordstart > temp_l)
			{
				temp_l = i - wordstart;
				wordstart_m = wordstart;
			}
		}
		i++;
	}
	printf("Maximal word ");
	for (i = wordstart_m; i < wordstart_m + temp_l; ++i)
		printf("%c", str[i]);
	printf(" consists of %d letters \n",temp_l);

	return 0;
}