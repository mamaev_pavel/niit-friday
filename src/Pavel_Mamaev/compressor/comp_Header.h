struct SYM // ������������� �������
 {
  unsigned char ch; // ASCII-���
  float freq; // ������� �������������
  char code[256]; // ������ ��� ������ ����
  struct SYM *left; // ����� ������� � ������
  struct SYM *right; // ������ ������� � ������
 };



 typedef struct SYM TSYM;
 typedef TSYM *PSYM;


  struct SYM_prim   //��������� ���������
   {
	int data;
	float freq;
	int flag;//���������� ��� ����������
	struct SYM_prim *left;
	struct SYM_prim *right;
   };

  typedef struct SYM_prim TSYM_prim;
  typedef TSYM_prim *PSYM_prim;


  struct SYM* buildTree(TSYM *psym[], int N);
  PSYM_prim createThree(PSYM_prim root,int data,double symW);
  int printThree(PSYM_prim root);
  PSYM_prim printsortThree(PSYM_prim root,PSYM_prim max,PSYM_prim buf,int i);
  void makeCodes(TSYM *root);
  void repack(unsigned char *char_dec,unsigned char *str);

  unsigned char pack(unsigned char *buf);

	union CODE {
	 unsigned char ch;
	 struct {
		 unsigned short b1:1;
		 unsigned short b2:1;
		 unsigned short b3:1;
		 unsigned short b4:1;
		 unsigned short b5:1;
		 unsigned short b6:1;
		 unsigned short b7:1;
		 unsigned short b8:1;
	 } byte;
	};

	 void masPrint(int count);
