#include "comp_Header.h"
#include <stdio.h>
#include <stdlib.h>

  TSYM syms[256];
  PSYM psym[256];
  static int char_count;
  static int j;
  extern *sym_mas=0;   //������ ������������� �������� ��� ���������

  //��������� ��������� �����

PSYM_prim createThree(PSYM_prim root,int data,double symW)
 {

 if (data==-1)
  return root;

  if( (root==0))
   {
	root=(PSYM_prim)malloc(sizeof(TSYM_prim));
	root->data=data;
	root->freq=symW;
	root->flag=0;
	root->left=0;
	root->right=0;
   }
  else if (data<root->data)
	root->left=createThree(root->left,data,symW);
  else if (data>root->data)
	root->right=createThree(root->right,data,symW);
  else
	{
	 root->freq=root->freq+symW;
	}
 return root;
 }


  int printThree(PSYM_prim root)   //�����
   {
   if(root)
	  {
	   printThree(root->left);
	   printf("%d  (%.5f)\n",root->data,root->freq)&&char_count++;
       printThree(root->right);

	  }
	return char_count;
   }


  PSYM_prim printsortThree(PSYM_prim root,PSYM_prim max,PSYM_prim buf,int i)   //������������ ����� � �����������
   {
	if(root->left)
	  printsortThree(root->left,max,buf,i);

	if ((root->freq>max->freq))
	  {
		max=root;
		printsortThree(buf,max,buf,i);

		if (root->flag!=1)
		 {
		  printf("%d  %.5f\n",max->data, max->freq);
		  syms[i].ch=max->data;
		  syms[i].freq=max->freq;    //������ � ��������� syms
		  syms[i].code[0]=0;
		  syms[i].left=0;
		  syms[i].right=0;
		  psym[i]=syms+i;
		  i++;

		  root->flag=1;
		  root->freq=0.0;
		 }

		 if (i==char_count)
		   {
			  buildTree(psym,char_count);
			  makeCodes(psym[0]);
			  puts("");
			  puts("Sorted with code");

			for (i=0; i < char_count; i++)
			 {
			   printf("%c %s\n",syms[i].ch,syms[i].code);
			 }
			return root;
		   }

		printsortThree(buf,max,buf,i);
	   }

	if(root->right)
	  printsortThree(root->right,max,buf,i);

  }


//��������� ���������
PSYM buildTree(TSYM *psym[],int N)
  {
	int i;
	PSYM buf[1]; //�������� ���������

	struct SYM *temp=(TSYM*)malloc(sizeof(TSYM));// ������ ��������� ����
	temp->freq=(psym[N-1])->freq+(psym[N-2])->freq;// � ���� ������� ������������ ����� ������ ���������� � �������������� ��������� ������� psym

	temp->left=psym[N-1];// ��������� ��������� ���� � ����� ���������� ������
	temp->right=psym[N-2];
	temp->code[0]=0;

	if(N==2) // �� ������������ �������� ������� � �������� 1.0
	 {
	  psym[0]=temp;
	  return temp;
	 }

// ��������� temp � ������ ������� psym,
// �������� ������� �������� �������
	for (i = N-3; i >=0; i--)
	  {
	   if (temp->freq>psym[i]->freq)
		  {
		   buf[0]=temp;
		   psym[i+1]=psym[i];
		   psym[i]=buf[0];
		   break;
		  }
	//   else
	   //	  psym[N-2]=temp;
	  }
	return buildTree(psym,N-1);
  }


void makeCodes(TSYM *root)
 {
   if(root->left)
	{
	 strcpy(root->left->code,root->code);
	 strcat(root->left->code,"0");
	 makeCodes(root->left);
	}
   if(root->right)
	{
	 strcpy(root->right->code,root->code);
	 strcat(root->right->code,"1");
	 makeCodes(root->right);
	}
 }

//-----------------------------------��������
 unsigned char pack(unsigned char *buf)
 {
   union CODE code;
   code.byte.b1=buf[0]-'0';
   code.byte.b2=buf[1]-'0';
   code.byte.b3=buf[2]-'0';
   code.byte.b4=buf[3]-'0';
   code.byte.b5=buf[4]-'0';
   code.byte.b6=buf[5]-'0';
   code.byte.b7=buf[6]-'0';
   code.byte.b8=buf[7]-'0';

   return code.ch;
 }


