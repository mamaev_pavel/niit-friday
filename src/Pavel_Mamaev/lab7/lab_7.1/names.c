#include "names.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

PITEM createList(PNAME_REC name_rec)
{
	PITEM item=(PITEM)malloc(sizeof(TITEM));
	item->name_rec=name_rec;
	item->prev=NULL;
	item->next=NULL;
	return item;
}
PNAME_REC createName(char *line)
{
	int i=0;
	PNAME_REC rec=(PNAME_REC)malloc(sizeof(TNAME_REC));
	while(*line && *line!=',')
		rec->name_country[i++]=*line++;
	rec->name_country[i]=0;
	line++;
	i=0;
	while(*line)
		rec->code_region[i++]=*line++;
	rec->code_region[i]=0;
	return rec;
}
PITEM addToTail(PITEM tail,PNAME_REC name_rec)
{
	PITEM item=createList(name_rec);
	if(tail!=NULL)
	{
		tail->next=item;
		item->prev=tail;
	}
	return item;
}
int countList(PITEM head)
{
	int count=0;
	while(head)
	{
		count++;
		head=head->next;
	}
	return count;
}
PITEM findByName(PITEM head,char *name)
{
	while(head)
	{
		if(strstr(head->name_rec->name_country,name)!=0)//������� strstr ��������� ���������
		   return head;

		head=head->next;
	}
	return NULL;
}

PITEM findByNameReg(PITEM head,char *name)
{
	while(head)
	{
		if(strstr(head->name_rec->name_region,name)!=0)//������� strstr ��������� ���������


		return head;
		head=head->next;
	}
	return NULL;
}


void printName(PITEM item)
{
	if(item!=NULL)
	{
	 puts(item->name_rec->code_region);
	}
}

void printNameReg(PITEM item)
{
	if(item!=NULL)
	{
	 puts(item->name_rec->name_country);
	}
}
