struct NAME_REC
{
	char name_country[4];
	char code_region[3];
	char name_region[50];
};

typedef struct NAME_REC TNAME_REC;
typedef TNAME_REC * PNAME_REC;

struct ITEM
{ 
	PNAME_REC name_rec;
	struct ITEM *next;
	struct ITEM *prev;
};

typedef struct ITEM TITEM;
typedef TITEM * PITEM;

PITEM createList(PNAME_REC name_rec);
PNAME_REC createName(char *line);
PITEM addToTail(PITEM tail,
			   PNAME_REC name_rec);
int countList(PITEM head);
PITEM findByName(PITEM head,char *name_country);
void printName(PITEM item);