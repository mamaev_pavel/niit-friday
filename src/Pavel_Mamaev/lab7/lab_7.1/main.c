//lab_7.1 ����� �������
#include "names.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main()
   {
	FILE *fp;
	int count=0;
	char buf[512];
	PITEM head,tail,item;
	fp=fopen("c://regions.csv","rt");

	if(!fp)
		{
		 perror("File names.csv:");
		 exit(1);
		}
	fgets(buf,512,fp);

	while(fgets(buf,512,fp))
	   {
		 if(count==0)
		   {
			head=createList(createName(buf));
			tail=head;
			}
		 else
		   {
			tail=addToTail(tail,createName(buf));
		   }
		count++;
	  }
	fclose(fp);
	
	printf("Total items: %d\n\n",countList(head));

	item=findByName(head,"RU") ;

	if(item==NULL)
	 printf("Not found!\n");
	else
	   {
		while ( item=findByName(item,"RU"))
		  {
		   printName(item);
		   item=item->next;
		  }
	   }

   puts("Information by region Kaluga");

   item=findByNameReg(head,"Kaluga") ;

   if(item==NULL)
	printf("Not found!\n");
   else
	   {

		while ( item=findByNameReg(item,"Kaluga"))
			 {
			  printNameReg(item);
			  item=item->next;
			 }
	   }

  do
   {
   puts("\nPress ENTER");
   }
   while (getchar()!='\n');
	return 0;
}