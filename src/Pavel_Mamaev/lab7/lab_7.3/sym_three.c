#include "sym_header.h"
#include <stdio.h>
#include <stdlib.h>


PSYM createThree(PSYM root,int data,float symW)
 {
 if (data==-1)
  return root;

  if( (root==0))
   {
	root=(PSYM)malloc(sizeof(TSYM));
	root->data=data;
	root->freq=symW;
	root->left=0;
	root->right=0;
   }
  else if (data<root->data)
	root->left=createThree(root->left,data,symW);
  else if (data>root->data)
	root->right=createThree(root->right,data,symW);
  else
	{
	 root->freq=root->freq+symW;
	}
 return root;
 }


  void printThree(PSYM root)   //������������ �����
   {
	 if(root)
	  {
	  printThree(root->left);
	  printf("%d   %c  (%.2f)\n",root->data,root->data,root->freq);
	  printThree(root->right);
	  }
   }


  PSYM printsortThree(PSYM root,PSYM max,PSYM buf)   //������������ �����
   {
	 if ((root->freq!=0.0)&&(root->freq>max->freq))
	   {
		max=root;
		printsortThree(buf,max,buf);

		root->freq!=0.0 && printf("%c  %.2f\n",max->data, max->freq);
		root->freq=0.0;

		printsortThree(buf,root,buf);
	   }

	 if(root->left)
		printsortThree(root->left,max,buf);

	 if(root->right)
		printsortThree(root->right,max,buf);

  }


