
   struct SYM

   {
	int data;
	float freq;
	struct SYM *left;
	struct SYM *right;
   };

  typedef struct SYM TSYM;
  typedef TSYM *PSYM;


  PSYM createThree(PSYM root,int data,float symW);
  void printThree(PSYM root);
