 #define N 10

  struct KeyWord
   {
	char data[N];
	int count;
	struct KeyWord *left;
	struct KeyWord *right;
   };

  typedef struct KeyWord TKeyWord;
  typedef TKeyWord *PKeyWord;

  void printThree(PKeyWord root);

  PKeyWord searchThree(PKeyWord root,char *data,int count);
  PKeyWord createThree(PKeyWord root,char *data,int count);


