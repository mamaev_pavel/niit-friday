#include "key_header.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 10

PKeyWord createThree(PKeyWord root,char *data,int count)
 {
  int i;
  if (root==0)
   {
	root=(PKeyWord)malloc(sizeof(TKeyWord));

	for (i = 0; i < N; i++)
	 {
	  root->data[i]=data[i];
	 }
	root->count=0;
	root->left=0;
	root->right=0;
   }
  else if (data[0]<root->data[0])
	root->left=createThree(root->left,data,count);

  else if (data[0]>root->data[0])
	root->right=createThree(root->right,data,count);

  else if ((data[0]==root->data[0])&&(data[1]==root->data[1]))
	{
	 root->count++;
	}
  return root;
 }


PKeyWord searchThree(PKeyWord root,char *data,int count)
 {
  if (data[0]<root->data[0])
	{
	 if (root->left)
		root->left=searchThree(root->left,data,count);
	 }

  else if (data[0]>root->data[0])
	{
	 if (root->right)
		root->right=searchThree(root->right,data,count);
	 }
  else if ((data[0]==root->data[0])&&(data[1]==root->data[1]))
	{
	 root->count++;
	}
   else
   return root;

 return root;

 }


  void printThree(PKeyWord root)   //������������ �����
   {
     if(root)
	  {
	   printThree(root->left);
	   printf("%s (%d)\n",root->data,root->count);
	   printThree(root->right);
	  }
   }


