/*
�������� ���������, ������� ��������� �������� ���� �� �����-
��� � �������, �, ��������, � ����������� �� ������� ��� �����.
��������: 45.00D - �������� �������� � ��������, � 45.00R - �
��������. ���� ������ �������������� �� ������� %f%c
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h> 

int main()
{
	float angle = 0;
	char unit = 0;
	float result = 0;

	printf("Enter the value of the angle (__.__D/R)\n");
	scanf("%f%c", &angle, &unit);

	if (unit == 'D')
	{
		result = (M_PI / 180) * angle;
		printf("%f\n", result);
	}
	else if (unit == 'R')
	{
		result = (angle / M_PI) * 180;
		printf("%f\n", result);
	}
	else
		printf("Sorry, repeat again\n");				//��������� �����
			

	return(0);
}
