/*
�������� ��������� ��� ������ ����� ������� ������������������� � ���-
���� � �������������� ���������� ������ �������� ����������.
���������:
���������� ����� �������� ������ ���� �������� � �������������� ��������
���, ����� ������ � ������ ������������� ����� ���������.
*/
#include <stdio.h>

char str[80];
char *p;
int i = 0, mem = 0, imem = 0, count = 1;

int main()
{
	printf("Enter an arbitrary string: \n");
	fgets(str, 80, stdin);
	p = str;

	for (; *p != '\n'; p++)
	{
		if (*p == *(p - 1))
		{
			count++;
		}
		else
		{
			if (mem < count)
			{
				mem = count;
				imem = *(p - 1);
			}

			count = 1;
		}
	}
	printf("%d ", mem);
	for (; mem>0; mem--)
		printf("%c", imem);
	return 0;
}