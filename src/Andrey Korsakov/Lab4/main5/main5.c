/*
�������� ���������, ����������� ������ (��. ������ 1), �� ������������
������, ����������� �� ���������� �����. ��������� ������ ��������� �����
������������ � ����.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define N 10
#define M 80

char str[N][M];
char *p[N];
int i = 0, count = 0;

int main()
{

	FILE *in, *out;
	in = fopen("input.txt", "r");
	out = fopen("output.txt", "w");

	while (i < N && *fgets(str[i], M, in) != '\n')
	{
		p[i] = str[i++];
	}
	count = i;

	i = 1;
	while (i < count)
	{
		if (i == 0 || strlen(p[i - 1]) <= strlen(p[i]))
			i++;
		else
		{
			char *temp = p[i];
			p[i] = p[i - 1];
			p[i - 1] = temp;
			i--;
		}
	}

	for (i = 0; i < count; i++)
	{
		fprintf(out, "%s", p[i]);
	}
	fclose(in);
	fclose(out);
	return 0;
}