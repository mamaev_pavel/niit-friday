/*
�������� ���������, ��������� ������� ������������� �������� ��� ������-
��� ������������� ������. � ���� ������� ���������� ������ ������ � �����
��� ����������.
*/
#include <stdio.h>
#include <string.h>
#define N 256

int main()
{
	char str[N];
	int arr[N] = { 0 };
	int i = 0;

	printf("Enter an arbitrary string: \n");
	fgets(str, N, stdin);
	str[strlen(str) - 1] = 0;

	do
	arr[str[i++]]++;
	while (str[i]);

	for (i = 0; i<N; i++)
	{
		if (arr[i] != 0)
			printf("%c = %d \n", i, arr[i]);
	}

	return 0;
}