/*
�������� ���������, �������������� ������� � ������� �������� �������:
������� ���� ��������� �����, ����� �����.������ �������� � ���� �������-
�� � ���� ��������� ������������������ ���� � ����. ������������ �������-
�������� ��������� ������.
*/
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main()
{
	int ran = 0;
	int res = 0;
	int i = 0;      //���-�� �������� ��������� �������������
	int j = 0;		//���-�� ��������
	int a = 0;	
	int b = 0;
	char str[81] = { 0 };

	srand(time(0));

	printf("Enter the number of characters from 1 to 80 \n");
	scanf("%d", &i);
	if (i > 0 && i <= 80)
	{
		for (j = 0; j < i; j++)
		{
			ran = rand() % 3;
			if (ran == 0)
			{
				ran = 48 + rand() % 10;
			}
			else if (ran == 1)
			{
				ran = 65 + rand() % 26;
			}
			else
			{
				ran = 97 + rand() % 26;
			}
			str[j] = ran;
			printf("%c", ran);
		}
		b = i - 1;
		while (i != a+(i-b))
		{
			if (str[a] >= 65 && str[a] <= 90 || str[a] >= 97 && str[a] <= 122)
				a++;
			else
			{
				if (str[b] >= 48 && str[b] <= 57)
					b--;
				else
				{
					res = str[a];
					str[a] = str[b];
					str[b] = res;
					a++;
				}
			}
		}
		printf("\n");
		printf("%s \n", str);
	}
	else
		printf("Enter again \n");
	
	return 0;
}
