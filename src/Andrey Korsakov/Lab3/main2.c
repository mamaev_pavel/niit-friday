/*
�������� ���������, ������� ��� �������� ������ ���������� ����������
���� � ������� ������ ����� �� ��������� ������ � ��� �����
���������:
����� ����������� ����� ����������� ��������.
*/
#include <stdio.h>

int main()
{
	char str[80] = { 0 };
	int i = 0;
	int count = 0;
	int counts = 0;
	int inWord = 0;
	printf("Enter an arbitrary string: \n");
	fgets(str, 80, stdin);

	while (str[i] != '\n')
	{
		if (str[i] == ' ' && inWord == 0)
			i++;
		else if (str[i] != ' ' && inWord == 0)
		{
			inWord = 1;
			count++;
			counts = 0;
			printf("%d) ", count);
		}
		else if (str[i] != ' ' && inWord == 1)
		{
			while (str[i] != ' ' && str[i] != '\n')
			{
				printf("%c", str[i]);
				i++; 
				counts++;
			}
			printf(" - %d\n", counts);
			inWord = 0;
		}
	}

	return 0;
}