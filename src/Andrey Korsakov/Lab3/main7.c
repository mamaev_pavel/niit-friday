/*
�������� ���������, ������� �������� ������� ������������� �������� ���
�������� ������, ��������������� �� �������� �������
���������:
������� ��������� ����� �������, ��� ������� ���� ����� ��������������
�������, � ����� �� ���� ��������.
*/

#include <stdio.h>

char str[80] = { 0 };
char simbol[256] = { 0 };
int i = 0;
int j = 0;
int max = 0;

int main()
{ 
	printf("Enter an arbitrary string: \n");
	fgets(str, 80, stdin);
	
	while (str[i] != '\n')
	{
		simbol[str[i]]++;
		i++;
	}

	while (j < 256)
	{
		if (simbol[j] > max)
			max = simbol[j];
		
		j++;
	}

	while (max != 0)
	{
		for (j = 0; j < 256; j++)
		{
			if (simbol[j] == max)
				printf("%c - %d\n", j, simbol[j]);
		}
		max--;
	}

	return 0;
}