/*
�������� ��������� ������������, ��������� �� ����� �����������,
������������ �� ����������� ������������� ��������� �*�.
����������� ����������� � ��������� ���������� �������, � �����
��� ����� � ����������� ���������� � ��������� ��� �����.
*/
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <windows.h>

#define N 10

void arr_filling(char str[][N], char n)		//���������� �������
{
	int i = 0, j = 0, ran = 0;
	srand(time(0));
	// ��������� ������ ���������
	for (i = 0; i < N; i++)
	{
		for (j = 0; j < N; j++)
			str[i][j] = ' ';
	}
	// ��������� ������ '*'
	for (i = 0; i < N / 2; i++)
	{
		for (j = 0; j < N / 2; j++)
		{
			ran = rand() % 3;
			if (ran % 2 == 0)
				str[i][j] = '*';
		}
	}
}

void copy_arr(char str[][N])
{
	int i = 0, j = 0;
	//������� ������
	for (i = 0; i < N / 2; i++)
	{
		for (j = 0; j < N / 2; j++)
			str[i][N-1-j] = str[i][j];
	}
	//������� ����
	for (i = 0; i < N /2 ; i++)
	{
		for (j = 0; j < N; j++)
			str[N-1-i][j] = str[i][j];
	}
}

void print_arr(char str[][N])		//������ �������
{
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
			printf("%c", str[i][j]);
		printf("\n");
	}
}

int main()
{
	char str[N][N];
	
	while (1)
	{		
		arr_filling(str, N);
		copy_arr(str);
		print_arr(str);
		printf("\n");
		Sleep(5000);
	}

	return 0;
}