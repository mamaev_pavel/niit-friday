/*
�������� ���������, ������� ������ ��������� ��������� ���� �
������������ �������� ����� � ������ ������
���������:
��������� ��������� ������������ ���������� ���� � ������ ��� ���������.
��� ������ ������ ���������� �������, ������������� � ������ ������ 1.
*/
#include <stdio.h>
#include <string.h>
#include <time.h>
#define N 10
#define M 80

int get_Word(char str[][M], char **word, int i)
{
	int j = 0, k = 0, inWord = 0;
	while (str[i][j] != '\n')
	{
		if (str[i][j] != ' ' && inWord == 0)
		{
			inWord = 1;
			word[k] = &str[i][j];
			k++;
		}
		else if ((str[i][j] == ' ') && inWord == 1)
		{
			inWord = 0;
		}

		j++;
	}
	return k;
}

void print_Word(char*word, FILE *out)
{
	while (*word != ' ' && *word != '\n')
		fprintf(out, "%c", *word++);
	fprintf(out, " ");
}

void rend_String(char **word, FILE *out, int k)
{
	for (; k > 0; k--)
	{
		srand(time(0));
		int n = rand() % k;
		print_Word(word[n], out);
		char *temp = word[k - 1];
		word[k - 1] = word[n];
		word[n] = temp;
	}
}

int main()
{
	char str[N][M];
	char *word[N];
	int i = 0;

	FILE *in, *out;
	in = fopen("input.txt", "r");
	out = fopen("output.txt", "w");
	if (in == 0 || out == 0)
	{
		perror("File error!");
		exit(1);
	}

	while (i < N && *fgets(str[i], M, in) != '\n')
	{
		rend_String(word, out, get_Word(str, word, i));
		fprintf(out, "\n");
		i++;
	}

	return 0;
}



















/*
#include <stdio.h>
#include <time.h>

#define N 10
#define M 80

int get_Word(char str[][M], char **word)
{
	int i = 0, j = 0, k = 0, inWord = 0;
	while (str[i][j] != '\n')
	{
		if (str[i][j] != ' ' && inWord == 0)
		{
			inWord = 1;
			word[k] = &str[i][j];
			k++;
		}
		else if ((str[i][j] == ' ') && inWord == 1)
		{
			inWord = 0;
		}

		j++;
	}
	return k;
}

void rend_String(char **word, int k)
{
	for (; k > 0; k--)
	{
		srand(time(0));
		int n = rand() % k;
		print_Word(word[n]);
		char *temp = word[k - 1];
		word[k - 1] = word[n];
		word[n] = temp;
	}
}

void print_Word(char*word)
{
	while (*word != ' ' && *word != '\n')
		putchar(*word++);
	putchar(' ');
}

int main()
{
	char str[N][M];
	char *p[N], *word[N];
	int i = 0, k = 0;

	FILE *in, *out;
	in = fopen("input.txt", "r");
	out = fopen("output.txt", "w");
	if (in == 0 || out == 0)
	{
		perror("File error!");
		exit(1);
	}

	rend_String(word, get_Word(str, word));
	
	while (i < N && *fgets(str[i], M, in) != '\n')
	{
		
		i++;
//		p[i] = str[i++];
	}
	i--;
	k = i;
	

	fclose(in);
	fclose(out);
	return 0;
}


*/




/*

# define _CRT_SECURE_NO_WARNINGS
# include <stdio.h>
# include <string.h>
# include <time.h>

# define Words 40
# define string 256

int getWords(char** pp, char* str)
{
	int i = 0, countWord = 0, inWord = 0;

	while (str[i] != '\n')
	{
		if (str[i] != ' ' && inWord == 0)
		{
			inWord = 1;
			pp[countWord] = str + i;
			countWord++;
		}
		else if (str[i] == ' ' && inWord == 1)
			inWord = 0;
		i++;
	}
	return countWord;
}

void RNDWord(char** pp, int i)
{
	if (i < 2) return 0;
	int rnd;
	char *buf;
	buf = pp[i - 1];
	rnd = rand() % (i);
	pp[i - 1] = pp[rnd];
	pp[rnd] = buf;
	i--;
	RNDWord(pp, i);
}

int main()
{
	char buf[string];
	FILE *fin, *fout;
	char *p[Words];
	int i = 0, countWord = 0;
	srand(time(0));

	//---open files

	fin = fopen("input.txt", "rt");
	fout = fopen("output.txt", "wt");
	if (fin == 0 || fout == 0)
	{
		perror("File error!");
		exit(1);
	}

	//---analis and output

	while (1)
	{
		for (int k = 0; (k < string) && (buf[k] != 0); k++)
			buf[k] = 0;
		fgets(buf, string, fin);
		if (buf[0] == 0)
			exit(0);
		if (buf[strlen(buf) - 1] != '\n')
		{
			buf[strlen(buf)] = '\n';
			buf[strlen(buf) + 1] = 0;
		}
		countWord = getWords(p, buf);
		RNDWord(p, countWord);
		for (int j = 0; j < countWord; j++)
		{
			while ((*p[j] != ' ') && (*p[j] != 0) && (*p[j] != '\n'))
			{
				fprintf(fout, "%c", *p[j]);
				p[j]++;
			}
			fprintf(fout, " ");
		}
		fprintf(fout, "\n");
	}
	_fcloseall;
	return 0;
}
*/