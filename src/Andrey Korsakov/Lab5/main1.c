/*
�������� ���������, ������� ��������� �� ������������ ������ �
������� �� �� �����, ��������� ����� � ��������� �������.
*/
#include <stdio.h>
#include <string.h>
#include <time.h>
#define N 80

int get_Word(char *str, char **word)
{
	int i = 0, j = 0, inWord = 0;
	while (str[i] != '\n')
	{
		if (str[i] != ' ' && inWord == 0)
		{
			inWord = 1;
			word[j] = &str[i];
			j++;
		}
		else if ((str[i] == ' ') && inWord == 1)
		{
			inWord = 0;
		}

		i++;
	}
	return j;
}

void print_Word(char*word)
{
	while (*word != ' ' && *word != '\n')
		putchar(*word++);
	putchar(' ');
}

void rend_String(char **word, int j)
{
	for (; j > 0; j--)
	{
		srand(time(0));
		int n = rand() % j;
		print_Word(word[n]);
		char *temp = word[j - 1];
		word[j - 1] = word[n];
		word[n] = temp;
	}
}

int main()
{
	char str[N];
	char *word[N];

	printf("Enter an arbitrary string: \n");
	fgets(str, N, stdin);

	rend_String(word, get_Word(str, word));

	return 0;
}