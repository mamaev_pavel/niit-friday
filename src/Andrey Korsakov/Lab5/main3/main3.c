/*
�������� ���������, �������������� ��������� ������� �������
������� ����� ������ ������ ���������� �����, ����� �������
� ����������, �� ���� ������ � ����� ����� �������� �� ������.
*/

#include <stdio.h>
#include <time.h>

#define N 10
#define M 80

void rend_Word(char *t[], int count)
{
	int temp = 0, n = 0;
	for (count -= 2; count > 0; count--)
	{
		srand(time(0));
		n = rand() % count;
		temp = *(t[0] + 1);
		*(t[0] + 1) = *(t[0] + 1 + n);
		*(t[0] + 1 + n) = temp;
	}
}

void get_Word(char str[][M], int k)
{
	char *t[N];
	int i = 0, j = 0, inWord = 0, count = 0;

	for (i = 0; i <= k; i++)
	{
		while (str[i][j] != '\n')
		{
			if (str[i][j] != ' ' && inWord == 0)
			{
				t[0] = &str[i][j];
				inWord = 1;
				count++;
				j++;
			}
			else if (str[i][j] != ' ' && inWord == 1)
			{
				count++;
				j++;
			}
			else if ((str[i][j] == ' ') && inWord == 1)
			{
				rend_Word(t, count);
				count = 0;
				inWord = 0;
				j++;
				t[0] = 0;
			}
		}
		rend_Word(t, count);
		count = 0;
		inWord = 0;
		j = 0;
		t[0] = 0;
	}
}

void print_Word(char**p, FILE *out, int k)
{
	for (int i = 0; i <= k; i++)
	{
		fprintf(out, "%s", p[i]);
	}
}

int main()
{
	char str[N][M];
	char *p[N];
	int i = 0, k = 0;
	
	FILE *in, *out;
	in = fopen("input.txt", "r");
	out = fopen("output.txt", "w");
	if (in == 0 || out == 0)
	{
		perror("File error!");
		exit(1);
	}

	while (i < N && *fgets(str[i], M, in) != '\n')
	{
		p[i] = str[i++];
	}
	i--;
	k = i;

	get_Word(str, k);
	print_Word(p, out, k);
    
	fclose(in);
	fclose(out);
	return 0;
}



	


