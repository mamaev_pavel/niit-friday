#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

int main()
{
	int i = 0;
	int space = 0;
	char str[81];
	int len = 0;
	printf("Vvedite stroku: \n");
	fgets(str, 80, stdin);
	len = strlen(str) - 1;
	while (str[len - 1] == ' ')
	{
		len--;
	}
	while (str[i] == ' ')
	{
		i++;
	}
	while (i != len)
	{
		if (str[i] != ' ')
		{
			putchar(str[i]);
			i++;
			space = 0;
		}
		else if (str[i] == ' ' && space < 1)
		{
			putchar(str[i]);
			i++;
			space++;
		}
		else i++;
	}
	printf("-konec stroki\n");
	return 0;
}