#include <stdio.h>

int main()
{
	char pol;
	int rost = 0;
	int ves = 0;
	printf("Vvedite svoi pol, M ili J: ");
	scanf("%c", &pol);
	printf("Vvedite svoi rost: ");
	scanf("%d", &rost);
	printf("Vvedite svoi ves: ");
	scanf("%d", &ves);
	int raznicaM = rost - 100;
	int raznicaJ = rost - 110;
	if (pol == 'M' && ves > raznicaM)
		printf("Poxydet'");
	else if (pol == 'M' && ves < raznicaM)
		printf("Potolstet'");
	else if (pol == 'M' && ves == raznicaM)
		printf("Norma!");
	else if (pol !='M' && pol !='J')
		printf("Error!");
	if (pol == 'J' && ves > raznicaJ)
		printf("Poxydet'");
	else if (pol == 'J' && ves < raznicaJ)
		printf("Potolstet'");
	else if (pol == 'J' && ves == raznicaJ)
		printf("Norma!");

	puts("");
	return 0;
}