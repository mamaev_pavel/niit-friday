#include <stdio.h>

int main()
{
	char str[101];
	printf("Vvedite stroku: ");
	fgets(str, 100, stdin);
	int inWord = 0;
	int i = 0;
	int count = 0;
	while (str[i] != '\0')
	{
		if (str[i] != ' ' && inWord == 0)
		{
			count++;
			inWord = 1;
		}
		else if (str[i] == ' ' && inWord == 1)
			inWord = 0;
		i++;
	}
	printf("Kol-vo slov v stroke: %d ", count);
	return 0;
}