//�������� ���������, ������� � ������� ������� ���������� ������� ����� ������ � �������� �������

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 80
void printWord(char *p);
int getWords(char *str, char **p);

int main()
{
    int i = 0, j;
    char str[N], *p[N];
    printf("Vvedite stroku \n");
    fgets(str, 80, stdin);

    i = strlen(str) - 1;
    str[i] = ' ';

    j = getWords(str, p);

    for (i = j - 1; i > -1; --i)
    {
        printWord(p[i]);
    }
    return 0;
}
int getWords(char *str, char **p)
{
    int i = 0, j = 0, inWord = 0;
    while (str[i])
    {
        if (str[i] != ' ' && inWord == 0)
        {
            p[j] = str + i;
            j++;
            inWord = 1;
        }
        else if (str[i] == ' ' && inWord == 1)
            inWord = 0;
        i++;
    }
    return j;
}
void printWord(char *p)
{

    while (*p != ' ')
    {
        putchar(*p++);
    }
    putchar(' ');
}